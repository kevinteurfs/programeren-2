﻿using System;

using Blog;
using System.Collections.Generic;
//namespace: naam geven aan de namespace om klassen binnen te isoleren binnen de namespace (zoals familienaam)
namespace CSharpLerenViaConsole
{
    // public: zichtbaar buiten de klasse, private: niet zichtbaar
    class Program
    {
        static void GeefPostcodesIn()
        {
            string postcode;
            string stad;
            string provincie;
            string localite;
            string province;
            List<Postcode> postcodeList = new List<Postcode>();
            do
            {
                Console.Clear();

                Console.Write("Typ een postcode in:");
                postcode = Console.ReadLine();
                if (postcode == "Q")
                {
                    break;
                }
                Console.Write("Typ de naam van de stad in:");
                stad = Console.ReadLine();
                if (stad == "Q")
                {
                    break;
                }

                Console.Write("Typ de naam van de provincie in:");
                provincie = Console.ReadLine();
                if (provincie == "Q")
                {
                    break;
                }
                Console.Write("Typ de naam in het frans van de stad in:");
                localite = Console.ReadLine();
                if (localite == "Q")
                {
                    break;
                }
                Console.Write("Typ de naam in het frans van de provincie in:");
                province = Console.ReadLine();

                // ik wil de ingetypte gegevens van de postcode opslaan
                // waarin? Nu staan ze in string variabelen
                //telkens als er een nieuwe postcode wordt ingetypt
                //worden dezelfde stringvariaabelen gebruikt. Dus de vorige postode
                // wordt overschreven. Wat komt ons redden?
                // de postcode klasse. De klasse bevat een beschrijving van de postcode, dwz maakt
                // een veld en properties voor elk kenmerk van de postcode: Plaats, Stad, Provincie,Localite,Ville Province
                // ik moet nu eerst een object, een instatie maken van de klasse:
                Postcode objectPostcode = new Postcode();
                // ik wil de waarden die in de string variabelen staan in de velde van de objectpostcodeinstantie kopiëren
                // daarvoor gebruik ik de setters van de properties

                objectPostcode.Code = postcode;
                objectPostcode.Plaats = stad;
                objectPostcode.Provincie = provincie;
                objectPostcode.Localite = localite;
                objectPostcode.Province = province;
                //voor elke postcode maak ik een nieuw object, dwz dat de gegevens van de vorige postcode niet w overschreven
                //elk ingegeven postcode heeft zijn eigen object
                //
                // de instantie objectPostcode aan de generieke lijst toevoegen
                postcodeList.Add(objectPostcode);

            } while (postcode != "Q");

            foreach (Postcode postcodeItem in postcodeList)
            {
                Console.WriteLine("{0} {1} {2} {3} {4}, ",
                        postcodeItem.Code, postcodeItem.Plaats, postcodeItem.Provincie,
                        postcodeItem.Localite, postcodeItem.Province);
            }
            //beschik ik over een instantie van Postcode Klasse?
            //Nee, ik moet dus eerst een instantie van de Postcodeklasse maken
            Postcode mijnPostcode = new Postcode();
            string message = mijnPostcode.SerializeObjectToCsv(postcodeList, "$");
            Console.WriteLine(message);

        }

        static void GeefPersoongegevensIn()
        {
            string voornaam;
            string familienaam;
            string geslacht;
            string leeftijd;
            string adres;

            List<Persoon> PersoongegevensList = new List<Persoon>();
            do
            {
                Console.Clear();

                Console.Write("Type een voornaam in:");
                voornaam = Console.ReadLine();
                if (voornaam == "Q")
                {
                    break;
                }
                Console.Write("Type een familienaam in:");
                familienaam = Console.ReadLine();
                if (familienaam == "Q")
                {
                    break;
                }

                Console.Write("Type het geslacht van de persoon in:");
                geslacht = Console.ReadLine();
                if (geslacht == "Q")
                {
                    break;
                }
                Console.Write("Type de leeftijd van de persoon in:");
                leeftijd = Console.ReadLine();
                if (leeftijd == "Q")
                {
                    break;
                }
                Console.Write("Type het volledige adres in:");
                adres = Console.ReadLine();

                // ik wil de ingetypte gegevens van de postcode opslaan
                // waarin? Nu staan ze in string variabelen
                //telkens als er een nieuwe postcode wordt ingetypt
                //worden dezelfde stringvariabelen gebruikt. Dus de vorige postode
                // wordt overschreven. Wat komt ons redden?
                // de postcode klasse. De klasse bevat een beschrijving van de postcode, dwz maakt
                // een veld en properties voor elk kenmerk van de postcode: Plaats, Stad, Provincie,Localite,Ville Province
                // ik moet nu eerst een object, een instatie maken van de klasse:
                Persoon objectPersoongegevens = new Persoon();
                // ik wil de waarden die in de string variabelen staan in de velde van de objectpostcodeinstantie kopiëren
                // daarvoor gebruik ik de setters van de properties

                objectPersoongegevens.Voornaam = voornaam;
                objectPersoongegevens.Familienaam = familienaam;
                objectPersoongegevens.Geslacht = geslacht;
                objectPersoongegevens.Leeftijd = leeftijd;
                objectPersoongegevens.Adres = adres;
                //voor elke postcode maak ik een nieuw object, dwz dat de gegevens van de vorige postcode niet w overschreven
                //elk ingegeven postcode heeft zijn eigen object
                //
                // de instantie objectPersoongegevens aan de generieke lijst toevoegen
                PersoongegevensList.Add(objectPersoongegevens);

            } while (leeftijd != "Q");

            foreach (Persoon persoonItem in PersoongegevensList)
            {
                Console.WriteLine("{0} {1} {2} {3} {4}, ",
                        persoonItem.Voornaam, persoonItem.Familienaam, persoonItem.Geslacht,
                        persoonItem.Leeftijd, persoonItem.Adres);
            }
            //beschik ik over een instantie van Postcode Klasse?
            //Nee, ik moet dus eerst een instantie van de Postcodeklasse maken
            Persoon mijnPersoongegevens = new Persoon();
            string message = mijnPersoongegevens.SerializeObjectToCsv(PersoongegevensList, "$");
            Console.WriteLine(message);
        }
        static void Main(string[] args)
        {
            //Console.WriteLine("Hello Crescendo World!");
            //CSharpLerenViaConsole.WerkenMetGegevens.CharLerenGebruiken();
            //ShowAscii is niet static
            //eerst instatie maken
            //WerkenMetGegevens werkenMetGegevens = new WerkenMetGegevens();
            //werkenMetGegevens.ShowAllAsciiValues();

            //WerkenMetGegevens.showCultureInfo();
            //Console.ReadKey();
            //WerkenMetGegevens.showCultureInfo();
            //WerkenMetGegevens.stirngconcatinationVersusStringBuilder();
            //Console.ReadKey();
            //WerkenMetGegevens werkenmetGegevens = new WerkenMetGegevens();
            //werkenmetGegevens.WerkenMetStruct();
            //WerkenMetGegevens.TryOutListArray();
            //Vormen mijnVorm = new Vormen();
            //mijnVorm.Kleur = ConsoleColor.Red;
            //Console.WriteLine(Vormen.Lijn(7,'-',ConsoleColor.Blue));
            //Console.Write(Vormen.Lijn(1,'*'));
            //Console.Write(Vormen.Lijn(5, ' '));
            //Console.WriteLine(Vormen.Lijn(1, '*'));
            //Console.Write(Vormen.Lijn(1, '*'));
            //Console.Write(Vormen.Lijn(5, ' '));
            //Console.WriteLine(Vormen.Lijn(1, '*'));
            //Console.Write(Vormen.Lijn(1, '*'));
            //Console.Write(Vormen.Lijn(5, ' '));
            //Console.WriteLine(Vormen.Lijn(1, '*'));
            // Console.Write(Vormen.Lijn(1, '*'));
            //Console.Write(Vormen.Lijn(5, ' '));
            //Console.WriteLine(Vormen.Lijn(1, '*'));
            //Console.WriteLine(Vormen.Lijn(7));
            //Console.WriteLine();
            //Console.Write(Vormen.Lijn(0, ConsoleColor.Red));
            //Console.WriteLine(Vormen.Lijn(7));
            //Console.Write(Vormen.Lijn(1, '|'));
            //Console.Write(Vormen.Lijn(5, ' '));
            //Console.WriteLine(Vormen.Lijn(1, '*'));
            //Console.Write(Vormen.Lijn(1, '|'));
            //Console.Write(Vormen.Lijn(4, ' '));
            //Console.WriteLine(Vormen.Lijn(1, '*'));
            //Console.Write(Vormen.Lijn(1, '|'));
            //Console.Write(Vormen.Lijn(3, ' '));
            //Console.WriteLine(Vormen.Lijn(1, '*'));
            //Console.Write(Vormen.Lijn(1, '|'));
            //Console.Write(Vormen.Lijn(2, ' '));
            //Console.WriteLine(Vormen.Lijn(1, '*'));
            //Console.Write(Vormen.Lijn(1, '|'));
            //Console.Write(Vormen.Lijn(1, ' '));
            //Console.WriteLine(Vormen.Lijn(1, '*'));
            //Console.ReadKey();

            //Persoon afzender = new Persoon();
            //afzender.voornaam = "Mohamed";
            //afzender.familienaam = "El Farisi";
            //afzender.Leeftijd = 22;
            //Persoon ontvanger = new Persoon();
            //ontvanger.voornaam = "Mathilde";
            //ontvanger.familienaam = "De Coninck";
            //ontvanger.Leeftijd = 24;
            //Persoon ontvanger2 = new Persoon();
            //ontvanger2.voornaam = "Jan";
            //ontvanger2.familienaam = "Dewilde";
            //ontvanger2.Leeftijd = 23;
            //Persoon ontvanger3 = new Persoon();
            //ontvanger3.voornaam = "Ilse";
            //ontvanger3.familienaam = "Peeters";
            //ontvanger3.Leeftijd = 286;

            //string afzendertekst = afzender.ShowInfo();
            //Console.WriteLine(afzendertekst);
            //string ontvangertekst = ontvanger.ShowInfo();
            //Console.WriteLine(ontvangertekst);
            //string ontvanger2tekst = ontvanger2.ShowInfo();
            //Console.WriteLine(ontvanger2tekst);
            //string ontvanger3tekst = ontvanger3.ShowInfo();
            //Console.WriteLine(ontvanger3tekst);
            //Helpers.Tekstbestand mijnTekstbestand = new Helpers.Tekstbestand();
            //// de setter van de property om een waarde aan het private
            //// field toe te kennen
            //mijnTekstbestand.FileName = "Data\\Postcodes.csv";
            //// de getter van de property om de waarde uit het private field
            //// op te halen
            //Console.WriteLine("Melding: {0}", mijnTekstbestand.Melding);
            //// good practice: manipulation data inside class (encapsulation)
            //Console.WriteLine("De volledige bestandsnaam is {0}", mijnTekstbestand.FullName);
            //// bad practice: gegevensmanipulatie buiten de klasse in de calling program
            //Console.WriteLine("De volledige bestandsnaam is c:\\data\\{0}", mijnTekstbestand.FileName);
            //// de tekst moet ingelezen worden: dit doen we met de .lees methode
            //if (mijnTekstbestand.Lees())
            //{
            //    //Toont de ingelezen tekst          
            //    Console.WriteLine($"De tekst in het bestand is \n{mijnTekstbestand.Text}");
            //}
            //else
            //{
            //    Console.WriteLine($"Het bestand heet:\n {mijnTekstbestand.Melding}");

            //}
            //Console.WriteLine("Typ een postcode in: ");
            //Console.ReadLine();
            // Zoek de postcode

            // instantie maken van de klasse Postcode:
            // Postcode postcode = new Postcode();
            // Console.WriteLine(postcode.ReadPostcodersFromCSVFile());
            // postcode.GetPostcodeList();

            // GeefPostcodesIn();
            GeefPersoongegevensIn();

            //geefPersonenIn();
            Console.WriteLine("Klik op een toets om het programma te beëindigen");
            Console.ReadKey();


        }


    }
}

