﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharpLerenViaConsole.Meetkunde
{
    public struct Point
    {
        public readonly int x;
        public readonly int y;

        public Point(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
    }
}
