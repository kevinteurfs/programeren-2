﻿using System;
﻿using Helpers;

namespace PostcodeApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("De Postcode App");
            Tekstbestand postcodeCSV = new Tekstbestand("Data/postcode.csv");
            postcodeCSV.Lees();
            Console.WriteLine(postcodeCSV.Text);
            Console.ReadKey();
        }
    }
}
