using System;
using Wiskunde.Meetkunde;

namespace CSharpLerenViaConsole
{

    class Program
    {

        static void Main(string[] args)

        {

            //maken van een driehoek
            Console.Write(Vormen.Lijn(0, ConsoleColor.Blue));
            Console.WriteLine(Vormen.Lijn(6));
            Console.Write(Vormen.Lijn(5, ' '));
            Console.WriteLine(Vormen.Lijn(0, '*'));
            Console.Write(Vormen.Lijn(4, ' '));
            Console.WriteLine(Vormen.Lijn(1, '*'));
            Console.Write(Vormen.Lijn(3, ' '));
            Console.WriteLine(Vormen.Lijn(2, '*'));
            Console.Write(Vormen.Lijn(2, ' '));
            Console.WriteLine(Vormen.Lijn(3, '*'));
            Console.Write(Vormen.Lijn(1, ' '));
            Console.WriteLine(Vormen.Lijn(4, '*'));
            Console.WriteLine(Vormen.Lijn(5, '*'));

            //maken van een rechthoek 
            Console.Write(Vormen.Lijn(7, ConsoleColor.Magenta));
            Console.Write(Vormen.Lijn(1, '*'));
            Console.WriteLine(Vormen.Lijn(1, '*'));
            Console.Write(Vormen.Lijn(1, '*'));
            Console.Write(Vormen.Lijn(5, ' '));
            Console.WriteLine(Vormen.Lijn(1, '*'));
            Console.Write(Vormen.Lijn(1, '*'));
            Console.Write(Vormen.Lijn(5, ' '));
            Console.WriteLine(Vormen.Lijn(1, '*'));
            Console.Write(Vormen.Lijn(1, '*'));
            Console.Write(Vormen.Lijn(5, ' '));
            Console.WriteLine(Vormen.Lijn(1, '*'));
            Console.WriteLine(Vormen.Lijn(7));
            Console.WriteLine();

            Console.ReadKey();
        }


    }
}
